# EmberWidgetsRails

Ember Widgets (https://github.com/addepar/ember-widgets/) has a couple interesting plugins for ember, but it's terrible to integrate. It only provides a way to throw everything they've got. I only wanted one widget, so I created this gem.

This gem only provides integration for carousel widget, tested on ruby 2.0.0 and rails 4.1.4.

I've been trying to use select widget, but failed at the integration.

## Installation

Add this line to your application's Gemfile:

    gem "ember_widgets_rails", git: 'git://github.com/KillaPL/ember_widgets_rails.git'

And then execute:

    $ bundle

## Usage

If you want to use ember-widgets carousel, just add the gem to gemfile, and add

    #= require ember-widgets/carousel

to your application.js

At least that's what works for me; )

## Contributing

1. Fork it ( https://github.com/[my-github-username]/ember_widgets_rails/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
