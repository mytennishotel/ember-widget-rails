Ember.Widgets = Ember.Namespace.create()
Ember.Widgets.VERSION = '0.1.0'
Ember.libraries?.register 'Ember Widgets', Ember.Widgets.VERSION
